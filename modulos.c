/* Este programa pide datos de tipo numerico, al usuario para guardalos en las variables, para que luego
   que los pidan, aparezcan en pantalla.

   @author Rodriguez Soto Cristian El�as.
   @date 15 de marzo de 2021.
*/
#include <stdio.h>
#include <stdlib.h>

void entero()
{
    int datosEnt; /* Aqui se va a declarar una varaible de tipo valor numerico entero (que se pidio) */
    char caract; /* Se declara una variable de tipo caracter (que se piddio) */
    float datosDec; /* Declara variable de tipo numerico decimal */


    printf("Ingrese un valor numerico entero: \n"); /* Imprime el texto, pide que se coloque un valor numerico para que sea almacenado. */

    scanf("%d" ,&datosEnt); /* El dato que coloco el usuario se guarda en la variaable para un futuro. */

    printf("El valor entero que ingreso es: %d \n", datosEnt); /* Se imprime el valor que fue guardado en la variaable por el usuario anteriormente. */

    printf("Ingrese un valor numerico con punto decimal: \n"); /* se pide un valor numerico con punto decimal para ser almacenado, y tener que usarlo despues.  */

    scanf("%f" ,&datosDec); /* El dato se guarda en la variable correspondiente, para poder utilizarlo despues. */

    printf("El valor decimal que ingreso es: %f \n", datosDec); /* se imprime el valor que se dio con punto decimal*/

    printf("Ingrese un caracter:(como el ejemplo que se ve) %s \n"); /* Aqui se ingresa el caracter que se necesite*/

    scanf("%s" ,&caract); /* el caracter que se ingreso se guardara aqui */

    printf("El caracter que ingreso es: %s \n", &caract); /* aqui aparecera el caracter que se coloco */

    printf("Apriete la tecla ENTER para finalizar el programa.");

    return; /* Termina la secuencia del programa  */
}
