/* Este programa pide datos de tipo numerico, al usuario para guardalos en las variables, para que luego
   que los pidan, aparezcan en pantalla.

   @author Rodriguez Soto Cristian El�as.
   @date 15 de marzo de 2021.
*/
#include <stdio.h>
#include <stdlib.h>
void entero();
int main()
{
    entero();
    return 0; /* Termina la secuencia del programa  */
}
